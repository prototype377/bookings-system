/* eslint-disable @typescript-eslint/no-unused-vars */
import Vue from "vue"
import VueRouter, { RouteConfig } from "vue-router"
import VehicleDashboard from '../modules/vehicle-booking/components/VehicleDashboard.vue'
Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "home",
    redirect: { name: 'available-vehicles' },
  },
  {
    path: "/vehicles/available",
    name: "available-vehicles",
    component: VehicleDashboard,
  },
]

const router = new VueRouter({
  routes,
})

export default router
