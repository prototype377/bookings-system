export function ToDate(rawDate :string): Date {
    return new Date(Date.parse(rawDate))
}
export function ToDateString(dateIn :Date) :string {
    return dateIn.toISOString().split('T')[0]
}
