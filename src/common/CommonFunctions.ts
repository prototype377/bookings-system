import Vue from "vue"

export type Nullable<T> = T | null

export function notNull<T>(val: T | null): val is T {
    return val !== null
}

// Move to ConfigService
export function isTestMode(): boolean {
    return Vue.prototype.$testMode
}

// export function copyValues(target: any, source: any) {
//     Object.keys(target).forEach(key => obj1[key] = obj2[key])
// }