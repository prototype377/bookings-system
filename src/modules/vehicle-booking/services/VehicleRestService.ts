import {isTestMode} from "@/common/CommonFunctions"
import axios, {AxiosResponse} from 'axios'
import AvailableVehicleMockData from "../mock-data/AvailableVehicleMockData.json"
import {Vehicle} from "../VehicleModel"


export const VEHICLE_REST_SERVICE = Symbol()

export class VehicleRestService {

    header: any = {
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json"
        },
        responseType: "json",
    }

    getAllAvaliableVehicles(): Promise<Partial<AxiosResponse<Vehicle[]>>> {
        if (isTestMode()) {
            const data: any = AvailableVehicleMockData
            const payload: Partial<AxiosResponse<Vehicle[]>> = { data: data as Vehicle[] }

            return Promise.resolve(payload)
        } else {
            return axios.get(`${process.env.VUE_APP_HOST_URL}/vehicle`)
        }
    }
}