import {isTestMode} from "@/common/CommonFunctions"
import axios, {AxiosResponse} from 'axios'
import AllBookingsMockData from "../mock-data/AllBookingsMockData.json"
import {Booking} from "../BookingModel"
import {HireCalcRequest} from "./HireCostRestService"


export const BOOKING_REST_SERVICE = Symbol()


export interface BookingRequest extends HireCalcRequest {
    calcPrice :number,
}

export interface BookingResponse {
    successful: boolean
}


export class BookingRestService {

    getAllBookings(): Promise<Partial<AxiosResponse<Booking[]>>> {
        if (isTestMode()) {
            const data: any = AllBookingsMockData
            const payload: Partial<AxiosResponse<Booking[]>> = { data: data as Booking[] }

            return Promise.resolve(payload)
        } else {
            return axios.get(`${process.env.VUE_APP_HOST_URL}/bookings`)
        }
    }

    createBooking(request :BookingRequest): Promise<Partial<AxiosResponse<BookingResponse>>> {
        if (isTestMode()) {
            const response: BookingResponse = {
                successful: true
            }

            const payload :Partial<AxiosResponse<BookingResponse>> = { data: response }
            return Promise.resolve(payload)
        } else {
            return axios.post(`${process.env.VUE_APP_HOST_URL}/bookings`, request)
        }
    }
}