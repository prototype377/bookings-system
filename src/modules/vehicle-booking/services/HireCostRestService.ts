import {isTestMode} from "@/common/CommonFunctions"
import axios, {AxiosResponse} from 'axios'
import moment from 'moment'


export const HIRE_COST_REST_SERVICE = Symbol()

export interface HireCalcRequest {
    regNo :string, 
    ratePerDay :number,
    startDate :Date,
    endDate :Date,
}

export interface HireCalcResponse {
    calculatedDateTime :Date,
    hirePrice :number
}

export class HireCostRestService {
    
    calcHireCost(request :HireCalcRequest): Promise<Partial<AxiosResponse<HireCalcResponse>>> {
        if (isTestMode()) {
            const admission = moment(request.startDate, 'YYYY-MM-DD')
            const discharge = moment(request.endDate, 'YYYY-MM-DD')
            const calcDays :number = discharge.diff(admission, 'days')
            const days :number = 1 + Math.abs(calcDays) // Hack issue in ui
            const price = days*request.ratePerDay
            
            const response: HireCalcResponse = {
                calculatedDateTime: new Date(),
                hirePrice: days*request.ratePerDay
            }

            const payload :Partial<AxiosResponse<HireCalcResponse>> = { data: response }
            return Promise.resolve(payload)
        } else {
            return axios.post(`${process.env.VUE_APP_HOST_URL}/calculation/hire`, request)
        }
    }
}