
export enum FuelType {
    DIESEL,
    PETROL
}

export interface Vehicle {
    regNo: string,
    category: string,
    make: string,
    model: string,
    fuel: FuelType
    ratePerDay: number,
}


export class Van implements Vehicle {

    category: string
    ratePerDay: number

    constructor(public regNo: string,
                public make: string,
                public model: string,
                public fuel: FuelType) {
        this.category = 'Van'
        this.ratePerDay = 50.0
    }
}
