
export interface BookingDate {
    start: string,
    end: string,
}

export interface Booking {
    regNo: string,
    dates: BookingDate[]
}
