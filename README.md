# Hire Booking System

# Setup

Install the application using `npm install` to install the required node packages.

Run the program locally using `npm run serve`, this will spin up a local server for the front-end.

Click on the link http://localhost:8081/ to open a browser to the page the application will be displayed.

# Issues

1. Rest controllers are coupled with test data, ideally separate test controllers for mocked data or mock expressJs Server instead.
2. Would not submit a calc price from the front-end as the price to book.
3. Vue Property Decorator is [DEPRECATED] replace with vue-facing-decorator
4. Date Picker order not enforced.
5. Row click functionality isn't right.

# Future Improvements

1. Migrate the vehicles and bookings to a VueX store.
2. Websockets in the main appliation receiving Booking updates and populating the VueX store.
3. There is a better Vuetify Calendar/Organiser that will allow to the user to see the models booked over weeks.
